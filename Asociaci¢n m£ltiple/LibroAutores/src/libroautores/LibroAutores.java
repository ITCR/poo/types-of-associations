package libroautores;

import java.time.LocalDate;
import java.time.Month;

public class LibroAutores {
    public static void main(String[] args) {
        Autor autor1 = new Autor("Stephen King", "Estados Unidos", LocalDate.of(1947, 9, 21), null);
        Autor autor2 = new Autor("Peter Straub", "Estados Unidos", LocalDate.of(1943, 3, 2), null);
        Autor autor3 = new Autor("J.R.R. Tolkien", "Reino Unido", LocalDate.of(1892, 1, 3), LocalDate.of(1973, 9, 2));

        Libro libro1 = new Libro("El Talismán", 375507779, LocalDate.of(2001, 9, 15), "Random House");
        libro1.agregarAutor(autor1);
        libro1.agregarAutor(autor2);
        
        Libro libro2 = new Libro("La Milla Verde", 451933028, LocalDate.of(1996, 9, 1), "Penguin Signet");
        libro2.agregarAutor(autor1);
        
        Libro libro3 = new Libro("El Hobbit", 618260307, LocalDate.of(1937, 9, 21), "Houghton Miffin");
        libro3.agregarAutor(autor3);
        
        Libro libro4 = new Libro("Julia", 345438655, LocalDate.of(2000, 7, 5), "Ballantine Books");
        libro4.agregarAutor(autor2);
        
        System.out.println(libro1.toString());
        System.out.println(libro2.toString());
        System.out.println(libro3.toString());
        System.out.println(libro4.toString());
    }
}
