package libroautores;

import java.time.LocalDate;

public class Autor {
    private String nombre;
    private String pais;
    private LocalDate fechaNacimiento;
    private LocalDate fechaDefuncion;
    
    public Autor(String nombre, 
                 String pais, 
                 LocalDate fechaNacimiento,
                 LocalDate fechaDefuncion) {
        this.nombre = nombre;
        this.pais = pais;
        this.fechaNacimiento = fechaNacimiento;
        this.fechaDefuncion = fechaDefuncion;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getPais() {
        return pais;
    }
    public void setPais(String pais) {
        this.pais = pais;
    }
    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }
    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }
    public LocalDate getFechaDefuncion() {
        return fechaDefuncion;
    }
    public void setFechaDefuncion(LocalDate fechaDefuncion) {
        this.fechaDefuncion = fechaDefuncion;
    }
    public String toString() {
        String resultado;
        resultado = "AUTOR: Nombre=" + nombre;
        resultado += " País=" + pais;
        resultado += " Nac=" + fechaNacimiento.toString();
        if (fechaDefuncion ==  null) {
            resultado += " Def=NA";
        } else {
            resultado += " Def=" + fechaDefuncion.toString();
        }
        return resultado;
    }
}
