package empleadocompania;

public class EmpleadoCompania {
    public static void main(String[] args) {
        Compania c1 = new Compania("INVU", "Barrio Amón");
        Compania c2 = new Compania("UCR", "San Pedro");
        c1.agregarEmpleado(new Empleado("Debra Pina", "722584552"));
        c1.agregarEmpleado(new Empleado("Ellis Gaona", "629390797"));
        c2.agregarEmpleado(new Empleado("Nolan Adame", "291028005"));
        c2.agregarEmpleado(new Empleado("Bruna Ponce", "126815701"));
        c2.agregarEmpleado(new Empleado("Vaniria Carrera", "304320455"));
        
        System.out.println(c1.toString());
        System.out.println(c2.toString());
    }
}
