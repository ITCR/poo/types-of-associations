package libroautores;

import java.time.LocalDate;
import java.util.ArrayList;

public class Libro {
    private String titulo;
    private int isbn;
    private LocalDate fechaPublicacion;
    private String editorial;
    private ArrayList<Autor> autores;

    public Libro(String titulo, 
                 int isbn, 
                 LocalDate fechaPublicacion, 
                 String editorial) {
        this.titulo = titulo;
        this.isbn = isbn;
        this.fechaPublicacion = fechaPublicacion;
        this.editorial = editorial;
        autores = new ArrayList();
    }
    public String getTitulo() {
        return titulo;
    }
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
    public int getIsbn() {
        return isbn;
    }
    public void setIsbn(int isbn) {
        this.isbn = isbn;
    }
    public LocalDate getFechaPublicacion() {
        return fechaPublicacion;
    }
    public void setFechaPublicacion(LocalDate fechaPublicacion) {
        this.fechaPublicacion = fechaPublicacion;
    }
    public String getEditorial() {
        return editorial;
    }
    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }
    public void agregarAutor(Autor autor) {
        autores.add(autor);
    }
    public ArrayList<Autor> getAutores() {
        return autores;
    }
    public String toString() {
        String resultado;
        resultado = "LIBRO: Título=" + titulo;
        resultado += " ISBN=" + isbn;
        resultado += " Pub=" + fechaPublicacion.toString();
        resultado += " editorial=" + editorial + "\n";
        
        for (Autor a : autores) {
            resultado += a.toString() + "\n";
        }
        return resultado;
    }
}
