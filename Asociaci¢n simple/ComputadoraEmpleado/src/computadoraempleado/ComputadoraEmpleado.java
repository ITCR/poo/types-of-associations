package computadoraempleado;

public class ComputadoraEmpleado {
    public static void main(String[] args) {
        Computadora compu1 = new Computadora("70406", "Lenovo X1 Carbon");
        Computadora compu2 = new Computadora("63854", "Toshiba Portege");
        compu1.setResponsable(new Empleado("Arana Marievio", 105260987));
        compu2.setResponsable(new Empleado("Rogerio Azurne", 203691458));
        System.out.println(compu1.toString());
        System.out.println(compu2.toString());        
    }
}
