package empleadocompania;

public class Empleado {
    private String nombre;
    private String cedula;
    private Compania empleador;
    
    public Empleado(String nombre, String cedula) {
        this.nombre = nombre;
        this.cedula = cedula;
        empleador = null;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getCedula() {
        return cedula;
    }
    public void setCedula(String cedula) {
        this.cedula = cedula;
    }
    public Compania getEmpleador() {
        return empleador;
    }
    public void setEmpleador(Compania empleador) {
        this.empleador = empleador;
    }
    public String toString() {
       String resultado = "EMPLEADO: Nombre=" + nombre;
       resultado += ", Cédula=" + cedula;
       resultado += ", Compania=" + empleador.getNombre();
       return resultado;
    }
}
