package empleadocompania;

import java.util.ArrayList;

public class Compania {
    private String nombre;
    private String ubicacion;
    private ArrayList<Empleado> empleados;

    public Compania(String nombre, String ubicacion) {
        this.nombre = nombre;
        this.ubicacion = ubicacion;
        empleados = new ArrayList();
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getUbicacion() {
        return ubicacion;
    }
    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }
    public void agregarEmpleado(Empleado empleado) {
        empleados.add(empleado);
        empleado.setEmpleador(this);
    }
    public Empleado getEmpleado(int posicion) {
        if (posicion >= 0 && posicion < empleados.size()) {
            return empleados.get(posicion);
        } else {
           return null;
        }
    }
    public String toString() {
        String resultado = "COMPAÑÍA: Nombre=" + nombre;
        resultado += ", Ubicacion=" + ubicacion + "\n";
        resultado += "Empleados:\n";
        for (Empleado e : empleados) {
            resultado += e.toString() + "\n";
        }
        return resultado;
    }
}
